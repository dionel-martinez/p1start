package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {

	@Override
	public int compare(Car car1, Car car2) {
		if(!car1.getCarBrand().equals(car2.getCarBrand())) return car1.getCarBrand().compareToIgnoreCase(car2.getCarBrand());
		if(!car1.getCarModel().equals(car2.getCarModel())) return car1.getCarModel().compareToIgnoreCase(car2.getCarModel());
		return car1.getCarModelOption().compareToIgnoreCase(car2.getCarModelOption());
		
	}

}

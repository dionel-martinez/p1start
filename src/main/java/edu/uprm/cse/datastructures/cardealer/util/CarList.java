package edu.uprm.cse.datastructures.cardealer.util;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CarList {
	/**
	 * Instance of the CSDLL<Car>
	 */
	private static CircularSortedDoublyLinkedList<Car> list = 
			new CircularSortedDoublyLinkedList<Car>(new CarComparator());

	/**
	 * Don't let anyone instantiate this class.
	 */
	private CarList() {}

	/**
	 * @return the instance of @list
	 */
	public static CircularSortedDoublyLinkedList<Car> getInstance(){
		return list;
	}

	/**
	 * Method that clears @list
	 */
	public static void resetCars() {
		list.clear();
	}

	/**
	 * @return A Car[] with all cars on @list
	 */
	public static Car[] toArray() {
		Car[] result = new Car[list.size()];
		int i = 0;
		for (Car car : list) {
			result[i] = car;
			i++;
		}
		return result;
	}
	
}

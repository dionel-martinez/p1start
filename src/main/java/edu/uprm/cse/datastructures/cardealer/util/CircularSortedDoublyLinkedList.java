package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author Dionel Martinez, dionel.martinez@upr.edu
 * @since 10/2019
 */
public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	private int currentSize;		//Actual Size of the list
	private Node<E> header;			//Dummy Header
	private Comparator<E> cmp;		//Comparator used in add(E e) method

	@SuppressWarnings("hiding")
	private class SortedCircularDoublyLinkedListIterator<E> implements Iterator<E>{

		Node<E> nextNode;

		@SuppressWarnings("unchecked")
		public SortedCircularDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
		}

		@Override
		public boolean hasNext() {
			return this.nextNode != header;
		}

		@Override
		public E next() {
			if(this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			throw new NoSuchElementException();
		}
		
	}

	private static class Node<E>{
		private E element;
		private Node<E> next;
		private Node<E> previous;

		public Node(E element, Node<E> previous, Node<E> next) {
			this.element = element;
			this.previous = previous;
			this.next = next;
		}

		public Node() {
			this(null, null, null);
		}

		/*
		 * Method that set all private fields of a Node to null
		 */
		public void clearNode() {
			this.element = null;
			this.previous = this.next = null;
		}
		
		/*
		 * Method that removes a Node appropriately from the sequence
		 */
		public void disappear() {
			Node<E> previous = this.getPrevious();
			this.getNext().setPrevious(previous);
			previous.setNext(this.getNext());
			this.clearNode();
		}

		public E getElement() {
			return this.element;
		}

		@SuppressWarnings("unused")
		public void setElement(E element) {
			this.element = element;
		}

		public Node<E> getNext() {
			return this.next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}

		public Node<E> getPrevious() {
			return this.previous;
		}

		public void setPrevious(Node<E> previous) {
			this.previous = previous;
		}
	}
	
	/**
	 * Constructor of the Circular Sorted Doubly Linked List Class
	 * @param Comparator<E>
	 */
	public CircularSortedDoublyLinkedList(Comparator<E> cmp) {
		this.header = new Node<E>();
		this.header.setPrevious(this.header);
		this.header.setNext(this.header);
		this.cmp = cmp;
		this.currentSize = 0;
	}

	@Override
	public Iterator<E> iterator() {
		return new SortedCircularDoublyLinkedListIterator<E>();
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}
	
	/**
	 * @param Element e
	 * @return true if the list contains the element e, false if doesn't
	 */
	@Override
	public boolean contains(E e) {
		return this.firstIndex(e) >= 0;
	}

	/**
	 *  @param Element e
	 *  @return first index of an Element e
	 */
	@Override
	public int firstIndex(E e) {
		if(e == null) return -1;

		Node<E> temp = this.header.getNext();
		int i = 0;
		while(temp != this.header) {
			if(temp.getElement().equals(e)) return i;
			temp = temp.getNext();
			i++;
		}
		return -1;
	}
	/**
	 *  @param Element e
	 *  @return last index of an Element e
	 */
	@Override
	public int lastIndex(E e) {
		int i = this.currentSize - 1;
		Node<E> temp = this.header.getPrevious();
		while(temp != this.header) {
			if(temp.getElement().equals(e)) return i;
			temp = temp.getPrevious();
			i--;
		}
		return -1;
	}
	
	/**
	 *  @param Element e
	 *  @return true after an element is added at its corresponding position
	 */
	@Override
	public boolean add(E e) {
		if(this.isEmpty()) return this.add(e, 0);
		
		int i = 0;
		for (E element : this) {
			if(this.cmp.compare(element, e) > 0) {
				return this.add(e, i);
			}
			i++;
		}
		return this.add(e, this.currentSize);
	}

	/**
	 *  @param Element e, @param index
	 *  @return true after an element is added at a specific index
	 */
	private boolean add(E e, int index) {
		Node<E> temp = this.getNode(index);
		Node<E> newNode = new Node<E>(e, temp.getPrevious(), temp);
		temp.getPrevious().setNext(newNode);
		temp.setPrevious(newNode);
		this.currentSize++;
		return true;
	}
	
	/**
	 * @param index
	 * @return the Node at a specific index
	 */
	private Node<E> getNode(int index) {
		Node<E> result = this.header.getNext();
		for (int i = 0; i < index; i++, result = result.getNext());
		return result;
	}

	/**
	 * @param index
	 * @return element at a specific index
	 */
	@Override
	public E get(int index) {
		if(index < 0 || index >= this.currentSize) throw new IndexOutOfBoundsException();
		return this.getNode(index).getElement();
	}

	/**
	 * @param index
	 * @return true if element at a specific index was successfully removed from the list, false if wasn't
	 */
	@Override
	public boolean remove(int index) {
		if(index < 0 || index >= this.currentSize) throw new IndexOutOfBoundsException();
		this.getNode(index).disappear();
		this.currentSize--;
		return true;
	}

	/**
	 * @param Element e
	 * @return true if element was removed from list, false if not found
	 */
	@Override
	public boolean remove(E e) {
		int index = this.firstIndex(e);
		if(index < 0) return false;
		return this.remove(index);
	}
	
	/**
	 * @param Element e
	 * @return number of copies removed
	 */
	@Override
	public int removeAll(E e) {
		/**
		 * This implementation is O(n)
		 */
		int count = 0;
		for(Node<E> temp = this.header.getNext() ; temp != this.header ; temp = temp.getNext()) {
			if(temp.getElement().equals(e)) {
				Node<E> previous = temp.getPrevious();
				temp.disappear();
				temp = previous;
				count++;
			}
		}
		this.currentSize -= count;
		return count;
	}
	
	/**
	 * Method that makes the list empty
	 */
	@Override
	public void clear() {
		while(!this.isEmpty()) this.remove(0);
	}

	/**
	 * @return the first element of the list
	 */
	@Override
	public E first() {
		return this.get(0);
	}

	/**
	 * @return the last element of the list
	 */
	@Override
	public E last() {
		return this.header.getPrevious().getElement();
	}

	/**
	 * @return A E[] with all elements on this list
	 */
	@SuppressWarnings("unchecked")
	public E[] toArray() {
		E[] result = (E[]) new Object[this.currentSize];
		int i = 0;
		for (E e : this) {
			result[i] = e;
			i++;
		}
		return result;
	}

}

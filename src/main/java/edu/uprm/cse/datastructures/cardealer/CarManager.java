package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.util.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.JsonError;
import edu.uprm.cse.datastructures.cardealer.util.NotFoundException;

@Path("/cars")
public class CarManager {

	public static CircularSortedDoublyLinkedList<Car> cars = CarList.getInstance();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		return CarList.toArray();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		Car result = null;
		for (Car c : cars) {
			if(c.getCarId() == id) result = c;
		}
		if (result != null) {
			return result;
		} 
		throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){

		for (Car c : cars) {
			if(c.getCarId() == car.getCarId())
				return Response.status(Response.Status.CONFLICT).build();
		}
		cars.add(car);
		return Response.status(Response.Status.CREATED).build();
	}

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car){
		boolean updated = false;
		for (Car c : cars) {
			if(c.getCarId() == car.getCarId()) {
				updated = cars.remove(c);
				cars.add(car);
				break;
			}
		}
		return updated ? Response.status(Response.Status.OK).build() 
				: Response.status(Response.Status.NOT_FOUND).build();
	}     

	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){
		boolean deleted = false;
		for (Car c : cars) {
			if(c.getCarId() == id) {
				deleted = cars.remove(c);
				break;
			}
		}
		return deleted ? Response.status(Response.Status.OK).build() 
				: Response.status(Response.Status.NOT_FOUND).build();
	}

}
